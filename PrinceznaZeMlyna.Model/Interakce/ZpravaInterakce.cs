﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinceznaZeMlyna.Model.Interakce
{
    public class ZpravaInterakce : abstractInterakce
    {
        public ZpravaInterakce(string predejZpravu)
        {
            TypMetody = MetodyEnum.ZPRAVA;
            Obsah = new object[] { predejZpravu };
        }
    }
}