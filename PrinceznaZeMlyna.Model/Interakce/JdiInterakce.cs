﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinceznaZeMlyna.Model.Interakce
{
    public class JdiInterakce : abstractInterakce
    {

        public JdiInterakce(Mistnost jdiDo)
        {
            TypMetody = MetodyEnum.JDI;
            Obsah = new object[] { jdiDo };
        }
    }
}
