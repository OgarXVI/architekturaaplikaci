﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinceznaZeMlyna.Model.Interakce;
namespace PrinceznaZeMlyna.Model
{
    public static class HerniData
    {


        public static Mistnost Init()
        {
            //OBJEKTZ
            #region mistnosti
            Mistnost mZahrada = new Mistnost()
            {
                Nazev = "Zahrada",
                Popis = "Zde se začíná.",
            };

            Mistnost mKomanta = new Mistnost()
            {
                Nazev = "Komnata",
                Popis = "Tajná a skrytá komnata."
            };

            Mistnost mKrálovaHala = new Mistnost()
            {
                Nazev = "Hala",
                Popis = "Toto je hala samotného krále."
            };
            #endregion

            #region postavy
            Postava pPrincezna = new Postava()
            {
                Nazev = "Princezna",
                Popis = "Nemá ráda svého otce.",
            };
            Postava pKral = new Postava()
            {
                Nazev = "Král",
                Popis = "Může ti darovat meč."
            };
            #endregion

            #region predmety
            Predmet iDyka = new Predmet()
            {
                Nazev = "Dýka",
                Popis = "Předmět nalezený v komnatě královny!",
                Vaha = 0.4f
            };

            Predmet iMec = new Predmet()
            {
                Nazev = "Meč",
                Popis = "Darovaný králem.",
                Vaha = 1.0f
            };
            #endregion

            //PROPOJENÍ
            #region vlozeni Predmetu
            mKomanta.Predmety.Add(iDyka);
            #endregion

            #region vlozeni Postavy
            mKrálovaHala.Postavy.Add(pPrincezna);
            mZahrada.Postavy.Add(pKral);
            #endregion

            #region vlozeni Sousedni mistnosti
            mZahrada.SoudedniMistnosti.Add(mKrálovaHala);
            mKrálovaHala.SoudedniMistnosti.Add(mZahrada);
            mKomanta.SoudedniMistnosti.Add(mKrálovaHala);
            #endregion


            //INTERAKCE
            #region interakce postavy
            //caller.HerniInformace["InteractCounter"] je automaticky ++ při zavolani delegata (je to v HerniObjekt)
            pKral.interactDelegate += delegate (HerniObjekt caller)
            {
                List<abstractInterakce> list = new List<abstractInterakce>();
                list.Add(new ZpravaInterakce((caller.HerniInformace["InteractCounter"]).ToString()));
                if ((int)caller.HerniInformace["InteractCounter"] == 0)
                {
                    list.Add(new ZpravaInterakce(Properties.Resources.KralMsg0));
                }
                else if ((int)caller.HerniInformace["InteractCounter"] == 5)
                {
                    list.Add(new DejPredmetInterakce(iDyka));
                    list.Add(new ZpravaInterakce($"{Properties.Resources.KralMsgSpeakToPrinces}"));
                    pPrincezna.HerniInformace["SpokeWithKing"] = true;
                }
                else
                {
                    Random r = new Random();
                    int n = r.Next(0, 3);
                    switch (n)
                    {
                        case 0:
                            list.Add(new ZpravaInterakce(Properties.Resources.KralMsg1));
                            break;
                        case 1:
                            list.Add(new ZpravaInterakce(Properties.Resources.KralMsg2));
                            break;
                        case 2:
                            list.Add(new ZpravaInterakce(Properties.Resources.KralMsg3));
                            break;
                    }
                }
                return list;
            };

            pPrincezna.interactDelegate += delegate (HerniObjekt caller)
            {
                List<abstractInterakce> list = new List<abstractInterakce>();
                if (caller.HerniInformace.ContainsKey("ChamberOpen") && (bool)caller.HerniInformace["ChamberOpen"] == true)
                {
                    list.Add(new ZpravaInterakce(Properties.Resources.PrincesMsgChambersOpened));
                }
                else if (caller.HerniInformace.ContainsKey("SpokeWithKing") && (bool)caller.HerniInformace["SpokeWithKing"] == true)
                {
                    list.Add(new ZpravaInterakce(Properties.Resources.PrincesMsgSpoke));
                }
                else
                {
                    list.Add(new ZpravaInterakce(Properties.Resources.PrincesMsgNotSpoke));
                    list.Add(new OdhalLokaciInterakce(mKomanta));
                    caller.HerniInformace["ChamberOpen"] = true;
                }

                return list;
            };

            #endregion

            #region interakce predmet
            iDyka.interactDelegate += delegate (HerniObjekt caller)
            {
                List<abstractInterakce> list = new List<abstractInterakce>();
                list.Add(new ZpravaInterakce($"{"Dýka byla použita"}: {caller.Nazev}"));
                return list;
            };
            #endregion

            #region interakce mistnost
            mKomanta.interactDelegate += delegate (HerniObjekt caller)
            {
                List<abstractInterakce> list = new List<abstractInterakce>();
                list.Add(new ZpravaInterakce($"{Properties.Resources.EnterKomnataLabel}"));
                return list;
            };
            #endregion

            return mZahrada;
        }

    }
}