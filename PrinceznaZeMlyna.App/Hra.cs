﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinceznaZeMlyna.App.Prikazy;
using PrinceznaZeMlyna.Model;
using System.ComponentModel;

namespace PrinceznaZeMlyna.App
{
    public static class Hra
    {
        public static iView View { get; set; }

        private static Mistnost _aktualniMistnost;

        public static Mistnost AktualniMistnost
        {
            get { return _aktualniMistnost; }
            set { _aktualniMistnost = value; }
        }

        private static List<Predmet> _inventar;
        public static List<Predmet> Inventar
        {
            get { return _inventar; }
        }
        public static void PridejItem(Predmet item)
        {
            _inventar.Add(item);
        }

        public static bool Prikaz(iPrikaz prikaz)
        {
            if (prikaz != null)
                return prikaz.ProvedPrikaz();
            else return false;
        }

        public static void Init(iView view)
        {
            View = view;
            _aktualniMistnost = HerniData.Init();
            _inventar = new List<Predmet>();
        }
    }
}
