﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinceznaZeMlyna.App;
using PrinceznaZeMlyna.App.Prikazy;
using PrinceznaZeMlyna.Model;
namespace PrinceznaZeMlyna.Run
{
    class View : iView
    {
        public void Show(string zprava)
        {
            Console.WriteLine(zprava);
        }

        public void Play()
        {

            Mistnost mistnost = Hra.AktualniMistnost;
            Console.WriteLine();
            Console.WriteLine($"{Model.Properties.Resources.LocationLabel}: \n{mistnost.PopisSe()}");

            Console.Write($"{Model.Properties.Resources.YourItemsLabel}:");
            foreach (Predmet item in Hra.Inventar)
            {
                Console.Write($"{item.Nazev}, ");
            }

            Console.WriteLine();

            Console.Write($"{Model.Properties.Resources.CommandLabel}:\n {Model.Properties.Resources.DescriptionCmd_SPEAK}\n {Model.Properties.Resources.DescriptionCmd_GO}\n {Model.Properties.Resources.DescriptionCmd_PICK_ITEM}\n {Model.Properties.Resources.Cmd_STOP}");

            Console.WriteLine();

            Console.WriteLine();
            string prikaz = Console.ReadLine();

            Console.Clear();
            iPrikaz cmd = NakrajejPrikaz(prikaz);
            if (cmd == null)
            {
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"{Model.Properties.Resources.CommandErrorMsg}");
                Console.ResetColor();
            }
            else
            {
                Hra.Prikaz(cmd);
            }
            if (prikaz != Model.Properties.Resources.Cmd_STOP)
                Play();

        }

        private iPrikaz NakrajejPrikaz(string prikaz)
        {
            string[] prikazy = prikaz.Split(' ');
            if (prikazy[0] == Model.Properties.Resources.Cmd_SPEAK && prikazy.Count() == 2)
            {
                Postava postava = Hra.AktualniMistnost.Postavy.Find(x => x.Nazev.ToLower() == prikazy[1].ToLower());
                if (postava != null)
                    return new MluvPrikaz(postava);
            }
            else if (prikazy[0] == Model.Properties.Resources.Cmd_GO && prikazy.Count() == 2)
            {
                Mistnost mistnost = Hra.AktualniMistnost.SoudedniMistnosti.Find(x => x.Nazev.ToLower() == prikazy[1].ToLower());
                if (mistnost != null)
                    return new JdiPrikaz(mistnost);
            }
            else if (prikazy[0] == Model.Properties.Resources.Cmd_PICK_ITEM && prikazy.Count() == 2)
            {
                Predmet predmet = Hra.AktualniMistnost.Predmety.Find(x => x.Nazev.ToLower() == prikazy[1].ToLower());
                if (predmet != null)
                    return new SeberPredmetPrikaz(predmet);
            }
            return null;

            /*
            switch (prikazy[0])
            {
                //NELZE (case musi byt konstantni)
                case Model.Properties.Resources.Cmd_SPEAK:
                  
            }
            */
        }
    }
}
