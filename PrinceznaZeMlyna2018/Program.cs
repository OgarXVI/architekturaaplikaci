﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinceznaZeMlyna.App;
using PrinceznaZeMlyna.Model;
using System.Threading;
using System.Globalization;

namespace PrinceznaZeMlyna.Run
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("cs");
            View view = new View();
            Hra.Init(view);

            view.Play();

            Console.WriteLine(Model.Properties.Resources.EndGameLabel);

            Console.ReadKey();
        }
    }
}